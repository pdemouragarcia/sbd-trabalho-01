/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.persistencia;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Bruning
 */
public class CRUD {
    // <editor-fold defaultstate="collapsed" desc="Criar"> 
    /**
     * Salva um objeto mapeado no banco de dados
     *
     * @param obj
     * @return boolean se salvou ou não
     */
    public static Serializable criar(Object obj) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        Serializable retorno = new Serializable() {};
        boolean salvou = false;
        try {
            tx = session.getTransaction();
            tx.begin();
            retorno = session.save(obj);
            salvou = true;
            tx.commit();
            
            System.out.println("salvouuuuuuuuuuuuuuuuuuuuu!");
        } catch (HibernateException e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            salvou = false;
            System.out.println("nao salvouuuuuuuuuuuuu");
        } finally {
            session.close();
        }
        return retorno;
    }    
    public static Serializable criar2(Object obj, Object obj2) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        Serializable retorno = new Serializable() {};
        boolean salvou = false;
        try {
            tx = session.getTransaction();
            tx.begin();
            retorno = session.save(obj);
            retorno = session.save(obj2);
            salvou = true;
            tx.commit();
            
            System.out.println("salvouuuuuuuuuuuuuuuuuuuuu!");
        } catch (HibernateException e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            salvou = false;
            System.out.println("nao salvouuuuuuuuuuuuu");
        } finally {
            session.close();
        }
        return retorno;
    }
    // </editor-fold>
    
    
    
    // <editor-fold defaultstate="collapsed" desc="Atualizar"> 
    /**
     * Salva um objeto mapeado no banco de dados
     *
     * @param obj
     * @return boolean se salvou ou não
     */
    public static boolean atualizar(Object obj) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        boolean salvou = false;
        try {
            tx = session.getTransaction();
            tx.begin();
            session.merge(obj);
            salvou = true;
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            salvou = false;
        }
        return salvou;
    }
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="Excluir"> 
    public static boolean excluir(int codigo, Class type) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.getTransaction();
            tx.begin();
            Object object = session.get(type, codigo);
            session.delete(object);
            session.flush();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            return false;
        } finally {
            session.close();
        }
        return true;
    }
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="Pesquisar"> 
    public static Object buscarObjetoViaParametro(String parametro, Object valor, Class<?> classe) {
        Object objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();//cria uma transação para o hibernate conectar no banco
            Criteria criteria = session.createCriteria(classe);
            criteria.add(Restrictions.eq(parametro, valor));
            objeto = criteria.uniqueResult();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return objeto;
    }
    
    public static List<?> buscarListaObjetosViaParametro(String parametro, Object valor, Class<?> classe) {
        List<?> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();//cria uma transação para o hibernate conectar no banco
            Criteria criteria = session.createCriteria(classe);
            criteria.add(Restrictions.eq(parametro, valor));
            objetos = criteria.list();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return objetos;
    }
   
    public static List<?> buscarListaObjetosViaParametroLimitado(String parametro, Object valor, Class<?> classe) {
        List<?> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();//cria uma transação para o hibernate conectar no banco
            Criteria criteria = session.createCriteria(classe);
            criteria.add(Restrictions.eq(parametro, valor));
            criteria.setMaxResults(10);
            objetos = criteria.list();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return objetos;
    }
    
    public static Object buscarObjetoViaListaParametros(HashMap<String, Object> parametros, Class<?> classe) {
        Object objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();//cria uma transação para o hibernate conectar no banco
            Criteria criteria = session.createCriteria(classe);
            for (Map.Entry<String, Object> entry : parametros.entrySet()) {
                String campo = entry.getKey();
                Object valor = entry.getValue();
                criteria.add(Restrictions.eq(campo, valor));
            }
            objeto = criteria.uniqueResult();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return objeto;
    }
    
    public static List<?> buscarListaObjetosViaListaParametros(HashMap<String, Object> parametros, Class<?> classe) {
        List<?> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();//cria uma transação para o hibernate conectar no banco
            Criteria criteria = session.createCriteria(classe);
            for (Map.Entry<String, Object> entry : parametros.entrySet()) {
                String campo = entry.getKey();
                Object valor = entry.getValue();
                criteria.add(Restrictions.eq(campo, valor));
            }
            objetos = criteria.list();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return objetos;
    }
    
    public static List<?> buscarTodosObjetosTabela(Class<?> classe) {
        List<?> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();//cria uma transação para o hibernate conectar no banco
            Criteria criteria = session.createCriteria(classe);
            objetos = criteria.list();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return objetos;
    }
    
    public static List<?> buscarTodosObjetosTabelaLimitado(Class<?> classe) {
        List<?> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();//cria uma transação para o hibernate conectar no banco
            Criteria criteria = session.createCriteria(classe);
            criteria.setMaxResults(1);
            objetos = criteria.list();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return objetos;
    }
    
    public static List<?> buscarViaConsulta(String hql) {
        List<?> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setMaxResults(10);
            objetos = query.list();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return objetos;
    }
    
    public static Object selecionarUltimo( Class classe, String queryId){
        Object obj = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();//cria uma transação para o hibernate conectar no banco
            Criteria criteria = session.createCriteria(classe);
            criteria.addOrder(Order.desc(queryId));
            obj = criteria.list().get(0);
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return obj;
    }

    // </editor-fold>   
}
