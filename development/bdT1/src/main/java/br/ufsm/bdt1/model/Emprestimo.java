/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.model;

import br.ufsm.bdt1.persistencia.EmprestimoDao;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Bruning
 */
@Entity
@Table(name = "emprestimo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Emprestimo.findAll", query = "SELECT e FROM Emprestimo e")
    ,
    @NamedQuery(name = "Emprestimo.findByIdEmprestimo", query = "SELECT e FROM Emprestimo e WHERE e.idEmprestimo = :idEmprestimo")
    ,
    @NamedQuery(name = "Emprestimo.findByDataRetirada", query = "SELECT e FROM Emprestimo e WHERE e.dataRetirada = :dataRetirada")
    ,
    @NamedQuery(name = "Emprestimo.findByDataDevolucao", query = "SELECT e FROM Emprestimo e WHERE e.dataDevolucao = :dataDevolucao")})
public class Emprestimo implements Serializable {

    @Size(max = 10)
    @Column(name = "data_limite_devolucao")
    private String dataLimiteDevolucao;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idEmprestimo")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idEmprestimo;
    @Size(max = 10)
    @Column(name = "data_retirada")
    private String dataRetirada;
    @Size(max = 10)
    @Column(name = "data_devolucao")
    private String dataDevolucao;
    @JoinColumn(name = "Livro_idLivro", referencedColumnName = "idLivro")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Livro livroidLivro;
    @JoinColumn(name = "Usuario_idUsuario", referencedColumnName = "idUsuario")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Usuario usuarioidUsuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "emprestimoidEmprestimo")
    private Collection<Multa> multaCollection;

    public Emprestimo() {
    }

    public Emprestimo(Integer idEmprestimo) {
        this.idEmprestimo = idEmprestimo;
    }

    public Integer getIdEmprestimo() {
        return idEmprestimo;
    }

    public void setIdEmprestimo(Integer idEmprestimo) {
        this.idEmprestimo = idEmprestimo;
    }

    public String getDataRetirada() {
        return dataRetirada;
    }

    public void setDataRetirada(String dataRetirada) {
        this.dataRetirada = dataRetirada;
    }

    public String getDataDevolucao() {
        return dataDevolucao;
    }

    public void setDataDevolucao(String dataDevolucao) {
        this.dataDevolucao = dataDevolucao;
    }

    public Livro getLivroidLivro() {
        return livroidLivro;
    }

    public void setLivroidLivro(Livro livroidLivro) {
        this.livroidLivro = livroidLivro;
    }

    public Usuario getUsuarioidUsuario() {
        return usuarioidUsuario;
    }

    public void setUsuarioidUsuario(Usuario usuarioidUsuario) {
        this.usuarioidUsuario = usuarioidUsuario;
    }

    @XmlTransient
    public Collection<Multa> getMultaCollection() {
        return multaCollection;
    }

    public void setMultaCollection(Collection<Multa> multaCollection) {
        this.multaCollection = multaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEmprestimo != null ? idEmprestimo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Emprestimo)) {
            return false;
        }
        Emprestimo other = (Emprestimo) object;
        if ((this.idEmprestimo == null && other.idEmprestimo != null) || (this.idEmprestimo != null && !this.idEmprestimo.equals(other.idEmprestimo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.ufsm.bdt1.model.Emprestimo[ idEmprestimo=" + idEmprestimo + " ]";
    }

    public String getDataLimiteDevolucao() {
        return dataLimiteDevolucao;
    }

    public void setDataLimiteDevolucao(String dataLimiteDevolucao) {
        this.dataLimiteDevolucao = dataLimiteDevolucao;
    }
    // <editor-fold defaultstate="collapsed" desc="Metodos Dao">

    public boolean cadastrar(Emprestimo a) {
        if (new EmprestimoDao().salvar(a) == true) {
            return true;
        } else {
            return false;

        }
    }

    public boolean alterar(Emprestimo a) {
        if (new EmprestimoDao().update(a) == true) {
            return true;
        } else {
            return false;
        }
    }

    public boolean excluir(Emprestimo a) {
        if (new EmprestimoDao().deletar(a) == true) {
            return true;
        } else {
            return false;
        }
    }

    //</editor-fold>
}
