/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.beans;

import br.ufsm.bdt1.model.Aluno;
import br.ufsm.bdt1.model.Emprestimo;
import br.ufsm.bdt1.model.Livro;
import br.ufsm.bdt1.model.Usuario;
import br.ufsm.bdt1.persistencia.CRUD;
import java.util.ArrayList;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Bruning
 */
public class novoEmprestimo {

    private Emprestimo novoEm;
    private ArrayList<Aluno> alunos;
    private Usuario usuarioSelecionado;
    private ArrayList<Livro> livros;
    private Livro livroSelecionado;
    private ArrayList<Usuario> usuarios;
    private ArrayList<Emprestimo> emprestimos;
    private Emprestimo excEmprestimo;

    /**
     * Creates a new instance of novoEmprestimo
     */
    public novoEmprestimo() {
        novoEm = new Emprestimo();
        usuarioSelecionado = new Usuario();
        alunos = new ArrayList<Aluno>();
        livroSelecionado = new Livro();
        livros = new ArrayList<Livro>();
        usuarios = new ArrayList<Usuario>();
        usuarios = (ArrayList<Usuario>) CRUD.buscarTodosObjetosTabela(Usuario.class);
        emprestimos = new ArrayList<Emprestimo>();
        excEmprestimo = new Emprestimo();
        this.buscarAlunos();
        this.buscarLivros();
        this.buscarEmprestimos();

    }

    private void buscarAlunos() {
        alunos = (ArrayList<Aluno>) CRUD.buscarTodosObjetosTabela(Aluno.class);
    }

    private void buscarLivros() {
        setLivros((ArrayList<Livro>) CRUD.buscarListaObjetosViaParametro("disponibilidade", true, Livro.class));
    }

    public void buscarEmprestimos() {
        setEmprestimos((ArrayList<Emprestimo>) CRUD.buscarTodosObjetosTabela(Emprestimo.class));
    }

    public void salvarEmprestimo() {
        if (novoEm.getIdEmprestimo() == null) {
            if (livroSelecionado == null && usuarioSelecionado == null) {
                error("Selecione um livro e um aluno!");
            } else {
                novoEm.setLivroidLivro(livroSelecionado);
                novoEm.setUsuarioidUsuario(usuarioSelecionado);
                novoEm.setDataDevolucao(novoEm.getDataLimiteDevolucao());
                CRUD.criar(novoEm);
                atencao("Emprestimo cadastrado com sucesso!");
            }
        } else {
            atualizarEmprestimo();
        }
        novoEm = new Emprestimo();
        livroSelecionado = new Livro();
        usuarioSelecionado = new Usuario();
    }
    
    public void atualizarEmprestimo(){
        CRUD.atualizar(novoEm);
        atencao("Emprestimo atualizado com sucesso!");
    }
    
    public void excluirEmprestimo() {
        CRUD.excluir(excEmprestimo.getIdEmprestimo(), excEmprestimo.getClass());
        atencao("Emprestimo excluído com sucesso!");
    }

    public void defEmprestimo() {
        livroSelecionado = novoEm.getLivroidLivro();
        usuarioSelecionado = novoEm.getUsuarioidUsuario();
    }

    public void error(String a) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro!", a));
    }

    public void atencao(String a) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Atenção!", a));
    }

    /**
     * @return the novoEm
     */
    public Emprestimo getNovoEm() {
        return novoEm;
    }

    /**
     * @param novoEm the novoEm to set
     */
    public void setNovoEm(Emprestimo novoEm) {
        this.novoEm = novoEm;
    }

    /**
     * @return the alunos
     */
    public ArrayList<Aluno> getAlunos() {
        return alunos;
    }

    /**
     * @param alunos the alunos to set
     */
    public void setAlunos(ArrayList<Aluno> alunos) {
        this.alunos = alunos;
    }

    /**
     * @return the usuarioSelecionado
     */
    public Usuario getUsuarioSelecionado() {
        return usuarioSelecionado;
    }

    /**
     * @param usuarioSelecionado the usuarioSelecionado to set
     */
    public void setUsuarioSelecionado(Usuario usuarioSelecionado) {
        this.usuarioSelecionado = usuarioSelecionado;
    }

    /**
     * @return the livros
     */
    public ArrayList<Livro> getLivros() {
        return livros;
    }

    /**
     * @param livros the livros to set
     */
    public void setLivros(ArrayList<Livro> livros) {
        this.livros = livros;
    }

    /**
     * @return the livroSelecionado
     */
    public Livro getLivroSelecionado() {
        return livroSelecionado;
    }

    /**
     * @param livroSelecionado the livroSelecionado to set
     */
    public void setLivroSelecionado(Livro livroSelecionado) {
        this.livroSelecionado = livroSelecionado;
    }

    /**
     * @return the usuarios
     */
    public ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }

    /**
     * @param usuarios the usuarios to set
     */
    public void setUsuarios(ArrayList<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    /**
     * @return the emprestimos
     */
    public ArrayList<Emprestimo> getEmprestimos() {
        return emprestimos;
    }

    /**
     * @param emprestimos the emprestimos to set
     */
    public void setEmprestimos(ArrayList<Emprestimo> emprestimos) {
        this.emprestimos = emprestimos;
    }

    /**
     * @return the excEmprestimo
     */
    public Emprestimo getExcEmprestimo() {
        return excEmprestimo;
    }

    /**
     * @param excEmprestimo the excEmprestimo to set
     */
    public void setExcEmprestimo(Emprestimo excEmprestimo) {
        this.excEmprestimo = excEmprestimo;
    }

}
