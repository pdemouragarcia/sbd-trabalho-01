/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.persistencia;

import br.ufsm.bdt1.model.Reserva;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class ReservaDao extends Dao implements Serializable {

    // <editor-fold defaultstate="collapsed" desc=" Buscar ">
    public ArrayList<Reserva> buscar() {
        return (ArrayList<Reserva>) buscarObjetos(Reserva.class);
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Inserir ">
    public boolean salvar(Reserva a) {
        return super.salvar(a);
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Remover ">
    public boolean deletar(Reserva a) {
        return excluir(a);
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Alterar ">
    public boolean update(Reserva a) {
        return super.update(a);
    }
    //</editor-fold>
}
