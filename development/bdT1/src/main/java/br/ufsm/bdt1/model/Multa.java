/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.model;

import br.ufsm.bdt1.persistencia.MultaDao;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bruning
 */
@Entity
@Table(name = "multa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Multa.findAll", query = "SELECT m FROM Multa m")
    ,
    @NamedQuery(name = "Multa.findByIdMulta", query = "SELECT m FROM Multa m WHERE m.idMulta = :idMulta")
    ,
    @NamedQuery(name = "Multa.findByValor", query = "SELECT m FROM Multa m WHERE m.valor = :valor")
    ,
    @NamedQuery(name = "Multa.findByDiasAtraso", query = "SELECT m FROM Multa m WHERE m.diasAtraso = :diasAtraso")})
public class Multa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idMulta")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idMulta;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor")
    private Float valor;
    @Column(name = "dias_atraso")
    private Integer diasAtraso;
    @JoinColumn(name = "Emprestimo_idEmprestimo", referencedColumnName = "idEmprestimo")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Emprestimo emprestimoidEmprestimo;

    public Multa() {
    }

    public Multa(Integer idMulta) {
        this.idMulta = idMulta;
    }

    public Integer getIdMulta() {
        return idMulta;
    }

    public void setIdMulta(Integer idMulta) {
        this.idMulta = idMulta;
    }

    public Float getValor() {
        return valor;
    }

    public void setValor(Float valor) {
        this.valor = valor;
    }

    public Integer getDiasAtraso() {
        return diasAtraso;
    }

    public void setDiasAtraso(Integer diasAtraso) {
        this.diasAtraso = diasAtraso;
    }

    public Emprestimo getEmprestimoidEmprestimo() {
        return emprestimoidEmprestimo;
    }

    public void setEmprestimoidEmprestimo(Emprestimo emprestimoidEmprestimo) {
        this.emprestimoidEmprestimo = emprestimoidEmprestimo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMulta != null ? idMulta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Multa)) {
            return false;
        }
        Multa other = (Multa) object;
        if ((this.idMulta == null && other.idMulta != null) || (this.idMulta != null && !this.idMulta.equals(other.idMulta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.ufsm.bdt1.model.Multa[ idMulta=" + idMulta + " ]";
    }
    // <editor-fold defaultstate="collapsed" desc="Metodos Dao">

    public boolean cadastrar(Multa a) {
        if (new MultaDao().salvar(a) == true) {
            return true;
        } else {
            return false;

        }
    }

    public boolean alterar(Multa a) {
        if (new MultaDao().update(a) == true) {
            return true;
        } else {
            return false;
        }
    }

    public boolean excluir(Multa a) {
        if (new MultaDao().deletar(a) == true) {
            return true;
        } else {
            return false;
        }
    }

    //</editor-fold>
}
