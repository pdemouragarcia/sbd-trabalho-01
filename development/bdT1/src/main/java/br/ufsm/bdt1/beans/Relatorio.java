/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.beans;

import br.ufsm.bdt1.persistencia.CRUD;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author pedro
 */
@ManagedBean(name = "relatorioMB")
@ViewScoped
public class Relatorio {

    private List list = new ArrayList();
    private String senha = "";
    @PostConstruct
    public void init() {
        list = new ArrayList();
    }

    public List<Relatorio> getList() {
        return list;
    }

    public void setList(List<Relatorio> list) {
        this.list = list;
    }

    public List<Relatorio> getQuery1() {
        QueryRelatorio1();
        return list;
    }

    public List<Relatorio> getQuery2() {
        QueryRelatorio2();
        return list;
    }

    public List<Relatorio> getQuery3() {
        QueryRelatorio3();
        return list;
    }

    public List<Relatorio> getQuery4() {
        queryRelatorio4();
        return list;
    }

    public List<Relatorio> getQuery5() {
        queryRelatorio5();
        return list;
    }

    public void QueryRelatorio1() {
        // Retornar todos os cursos cadastrados em alunos sem repetir e ordenar por ordem alfabetica (Uso do Distinct e do Order By)
        try {
            String url = "jdbc:mysql://localhost:3306/bd_t1?zeroDateTimeBehavior=convertToNull";
            String user = "root";
            Connection conn = DriverManager.getConnection(url, user, senha);
            Statement stmt = conn.createStatement();
            ResultSet rs;
            list.clear();

            rs = stmt.executeQuery("SELECT DISTINCT curso FROM aluno ORDER BY curso ASC");
            while (rs.next()) {
                String s = rs.getString("curso");
                list.add(s);

            }
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }

    }

    public void QueryRelatorio2() {
        //Retornar o nome de todos os usuarios que possuem idade entre 20 e 30 anos (Uso do Between)
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            String url = "jdbc:mysql://localhost:3306/bd_t1?zeroDateTimeBehavior=convertToNull";
            String user = "root";
            Connection conn = DriverManager.getConnection(url, user, senha);
            Statement stmt = conn.createStatement();
            ResultSet rs;
            list.clear();

            rs = stmt.executeQuery("SELECT nome FROM usuario WHERE nome LIKE 'a%' AND idade BETWEEN 20 AND 30 \n" +
"		UNION SELECT nome FROM usuario WHERE nome LIKE 'p%' AND idade BETWEEN 20 AND 30 ");
            while (rs.next()) {
                String s = rs.getString("nome");
                list.add(s);
            }
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }

    public void QueryRelatorio3() {
        //Retornar todos os livros que o titulo começa com a letra "A" (Uso do Like)
        try {
            String url = "jdbc:mysql://localhost:3306/bd_t1?zeroDateTimeBehavior=convertToNull";
            String user = "root";
            Connection conn = DriverManager.getConnection(url, user, senha);
            Statement stmt = conn.createStatement();
            ResultSet rs;
            list.clear();

            rs = stmt.executeQuery("SELECT titulo FROM livro WHERE titulo LIKE 'a%'");
            while (rs.next()) {
                String s = rs.getString("titulo");
                list.add(s);
            }
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }

    public void queryRelatorio4() {
        //list = CRUD.buscarViaConsulta("");
        try {
            String url = "jdbc:mysql://localhost:3306/bd_t1?zeroDateTimeBehavior=convertToNull";
            String user = "root";
            Connection conn = DriverManager.getConnection(url, user, senha);
            Statement stmt = conn.createStatement();
            ResultSet rs;
            list.clear();

            rs = stmt.executeQuery("SELECT nome FROM Usuario INNER JOIN Emprestimo on  Usuario.idUsuario = emprestimo.Usuario_idUsuario");
            while (rs.next()) {
                String s = rs.getString("nome");
                list.add(s);
            }
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }

    public void queryRelatorio5() {
        //list = CRUD.buscarViaConsulta("");
        try {
            String url = "jdbc:mysql://localhost:3306/bd_t1?zeroDateTimeBehavior=convertToNull";
            String user = "root";
            Connection conn = DriverManager.getConnection(url, user, senha);
            Statement stmt = conn.createStatement();
            ResultSet rs;
            list.clear();

            rs = stmt.executeQuery("SELECT area, COUNT(area) FROM Livro GROUP BY area HAVING COUNT(area)>1");
            while (rs.next()) {
                String s = rs.getString("area");
                list.add(s);
            }
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }
}
