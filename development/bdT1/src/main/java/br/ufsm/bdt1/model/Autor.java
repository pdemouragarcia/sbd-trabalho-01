/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.model;

import br.ufsm.bdt1.persistencia.AutorDao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Bruning
 */
@Entity
@Table(name = "autor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Autor.findAll", query = "SELECT a FROM Autor a")
    ,
    @NamedQuery(name = "Autor.findByIdAutor", query = "SELECT a FROM Autor a WHERE a.idAutor = :idAutor")
    ,
    @NamedQuery(name = "Autor.findByNome", query = "SELECT a FROM Autor a WHERE a.nome = :nome")
    ,
    @NamedQuery(name = "Autor.findByNacionalidade", query = "SELECT a FROM Autor a WHERE a.nacionalidade = :nacionalidade")})
public class Autor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idAutor")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idAutor;
    @Size(max = 100)
    @Column(name = "nome")
    private String nome;
    @Size(max = 80)
    @Column(name = "nacionalidade")
    private String nacionalidade;
    @JoinTable(name = "autor_livro", joinColumns = {
        @JoinColumn(name = "Autor_idAutor", referencedColumnName = "idAutor")}, inverseJoinColumns = {
        @JoinColumn(name = "Livro_idLivro", referencedColumnName = "idLivro")})
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Collection<Livro> livroCollection;

    public Autor() {
        livroCollection = new ArrayList<Livro>();
    }

    public Autor(Integer idAutor) {
        this.idAutor = idAutor;
    }

    public Integer getIdAutor() {
        return idAutor;
    }

    public void setIdAutor(Integer idAutor) {
        this.idAutor = idAutor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNacionalidade() {
        return nacionalidade;
    }

    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    @XmlTransient
    public Collection<Livro> getLivroCollection() {
        return livroCollection;
    }

    public void setLivroCollection(Collection<Livro> livroCollection) {
        this.livroCollection = livroCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAutor != null ? idAutor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Autor)) {
            return false;
        }
        Autor other = (Autor) object;
        if ((this.idAutor == null && other.idAutor != null) || (this.idAutor != null && !this.idAutor.equals(other.idAutor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.ufsm.bdt1.model.Autor[ idAutor=" + idAutor + " ]";
    }
    // <editor-fold defaultstate="collapsed" desc="Metodos Dao">

    public boolean cadastrar(Autor a) {
        if (new AutorDao().salvar(a) == true) {
            return true;
        } else {
            return false;

        }
    }

    public boolean alterar(Autor a) {
        if (new AutorDao().update(a) == true) {
            return true;
        } else {
            return false;
        }
    }

    public boolean excluir(Autor a) {
        if (new AutorDao().deletar(a) == true) {
            return true;
        } else {
            return false;
        }
    }

    //</editor-fold>
}
