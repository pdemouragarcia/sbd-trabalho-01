/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.persistencia;

import br.ufsm.bdt1.model.Usuario;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class UsuarioDao extends Dao implements Serializable {

    // <editor-fold defaultstate="collapsed" desc=" Buscar ">
    public ArrayList<Usuario> buscar() {
        return (ArrayList<Usuario>) buscarObjetos(Usuario.class);
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Inserir ">
    public boolean salvar(Usuario a) {
        return super.salvar(a);
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Remover ">
    public boolean deletar(Usuario a) {
        return excluir(a);
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Alterar ">
    public boolean update(Usuario a) {
        return super.update(a);
    }
    //</editor-fold>
}
