/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.persistencia;

import br.ufsm.bdt1.model.Emprestimo;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class EmprestimoDao extends Dao implements Serializable {

    // <editor-fold defaultstate="collapsed" desc=" Buscar ">
    public ArrayList<Emprestimo> buscar() {
        return (ArrayList<Emprestimo>) buscarObjetos(Emprestimo.class);
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Inserir ">
    public boolean salvar(Emprestimo a) {
        return super.salvar(a);
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Remover ">
    public boolean deletar(Emprestimo a) {
        return excluir(a);
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Alterar ">
    public boolean update(Emprestimo a) {
        return super.update(a);
    }
    //</editor-fold>
}
