/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.model;

import br.ufsm.bdt1.persistencia.AlunoDao;
import br.ufsm.bdt1.persistencia.UsuarioDao;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bruning
 */
@Entity
@Table(name = "aluno")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Aluno.findAll", query = "SELECT a FROM Aluno a")
    ,
    @NamedQuery(name = "Aluno.findByCurso", query = "SELECT a FROM Aluno a WHERE a.curso = :curso")
    ,
    @NamedQuery(name = "Aluno.findByMatricula", query = "SELECT a FROM Aluno a WHERE a.matricula = :matricula")
    ,
    @NamedQuery(name = "Aluno.findByAnoAdm", query = "SELECT a FROM Aluno a WHERE a.anoAdm = :anoAdm")
    ,
    @NamedQuery(name = "Aluno.findByIdAluno", query = "SELECT a FROM Aluno a WHERE a.idAluno = :idAluno")})
public class Aluno implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 45)
    @Column(name = "curso")
    private String curso;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "matricula")
    private Float matricula;
    @Column(name = "ano_adm")
    private Integer anoAdm;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idAluno")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idAluno;
    @JoinColumn(name = "Usuario_idUsuario", referencedColumnName = "idUsuario")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Usuario usuarioidUsuario;

    public Aluno() {
    }

    public Aluno(Integer idAluno) {
        this.idAluno = idAluno;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public Float getMatricula() {
        return matricula;
    }

    public void setMatricula(Float matricula) {
        this.matricula = matricula;
    }

    public Integer getAnoAdm() {
        return anoAdm;
    }

    public void setAnoAdm(Integer anoAdm) {
        this.anoAdm = anoAdm;
    }

    public Integer getIdAluno() {
        return idAluno;
    }

    public void setIdAluno(Integer idAluno) {
        this.idAluno = idAluno;
    }

    public Usuario getUsuarioidUsuario() {
        return usuarioidUsuario;
    }

    public void setUsuarioidUsuario(Usuario usuarioidUsuario) {
        this.usuarioidUsuario = usuarioidUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAluno != null ? idAluno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aluno)) {
            return false;
        }
        Aluno other = (Aluno) object;
        if ((this.idAluno == null && other.idAluno != null) || (this.idAluno != null && !this.idAluno.equals(other.idAluno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.ufsm.bdt1.model.Aluno[ idAluno=" + idAluno + " ]";
    }
    // <editor-fold defaultstate="collapsed" desc="Metodos Dao">

    public boolean cadastrar(Usuario u, Aluno a) {
        if (new UsuarioDao().salvar(u) == true) {
            if (new AlunoDao().salvar(a) == true) {
                return true;
            }else{
                return false;
            }
        } else {
            return false;

        }
    }

    public boolean alterar(Aluno a) {
        if (new AlunoDao().update(a) == true) {
            return true;
        } else {
            return false;
        }
    }

    public boolean excluir(Aluno a) {
        if (new AlunoDao().deletar(a) == true) {
            return true;
        } else {
            return false;
        }
    }

    //</editor-fold>
}
