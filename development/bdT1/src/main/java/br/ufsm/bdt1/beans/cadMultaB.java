/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.beans;

import br.ufsm.bdt1.model.Emprestimo;
import br.ufsm.bdt1.model.Multa;
import br.ufsm.bdt1.persistencia.CRUD;
import java.util.ArrayList;
import java.util.Objects;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Bruning
 */
public class cadMultaB {

    /**
     * Creates a new instance of cadMultaB
     */
    private ArrayList<Emprestimo> emprestimos = new ArrayList<Emprestimo>();
    private Multa multa;
    private ArrayList<Multa> multas = new ArrayList<Multa>();
    private Emprestimo emprestimoSelecionado;
    private Multa exMulta;
    
    public cadMultaB() {
        emprestimos = (ArrayList<Emprestimo>) CRUD.buscarTodosObjetosTabela(Emprestimo.class);
        multas = (ArrayList<Multa>) CRUD.buscarTodosObjetosTabela(Multa.class);
        multa = new Multa();
        emprestimoSelecionado = new Emprestimo();
        exMulta = new Multa();
    }

    public void cadastrar() {
        if (multa.getIdMulta() == null) {
            if (emprestimoSelecionado.getIdEmprestimo() != null) {
                multa.setEmprestimoidEmprestimo(emprestimoSelecionado);
                CRUD.criar(multa);
                atencao("Multa registrada com sucesso!");
            } else {
                error("Não foi possivel registrar a multa, preencha todos os campos!");
            }
        } else {
            if (Objects.equals(emprestimoSelecionado, multa.getEmprestimoidEmprestimo())) {
                editarMulta(multa);
            }else{
                multa.setEmprestimoidEmprestimo(emprestimoSelecionado);
                editarMulta(multa);
            }
        }
        emprestimoSelecionado = new Emprestimo();
        multa = new Multa();
    }
    
    public void editarMulta(Multa EMulta){
        CRUD.atualizar(EMulta);
        atencao("Multa Atualizada com sucesso!");
    }
    
    public void defMulta(){
        emprestimoSelecionado = multa.getEmprestimoidEmprestimo();
    }

    public void buscarMultas() {
        multas.clear();
        multas = (ArrayList<Multa>) CRUD.buscarTodosObjetosTabela(Multa.class);
    }

    public void excluirMulta() {
        if (exMulta != null) {
            CRUD.excluir(exMulta.getIdMulta(), exMulta.getClass());
            atencao("Multa excluída com sucesso");
        } else {
            atencao("Selecione uma multa para excluir");
        }
    }

    public void error(String a) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro!", a));
    }

    public void atencao(String a) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Atenção!", a));
    }

    /**
     * @return the emprestimos
     */
    public ArrayList<Emprestimo> getEmprestimos() {
        return emprestimos;
    }

    /**
     * @param emprestimos the emprestimos to set
     */
    public void setEmprestimos(ArrayList<Emprestimo> emprestimos) {
        this.emprestimos = emprestimos;
    }

    /**
     * @return the multa
     */
    public Multa getMulta() {
        return multa;
    }

    /**
     * @param multa the multa to set
     */
    public void setMulta(Multa multa) {
        this.multa = multa;
    }

    /**
     * @return the multas
     */
    public ArrayList<Multa> getMultas() {
        return multas;
    }

    /**
     * @param multas the multas to set
     */
    public void setMultas(ArrayList<Multa> multas) {
        this.multas = multas;
    }

    /**
     * @return the emprestimoSelecionado
     */
    public Emprestimo getEmprestimoSelecionado() {
        return emprestimoSelecionado;
    }

    /**
     * @param emprestimoSelecionado the emprestimoSelecionado to set
     */
    public void setEmprestimoSelecionado(Emprestimo emprestimoSelecionado) {
        this.emprestimoSelecionado = emprestimoSelecionado;
    }

    /**
     * @return the exMulta
     */
    public Multa getExMulta() {
        return exMulta;
    }

    /**
     * @param exMulta the exMulta to set
     */
    public void setExMulta(Multa exMulta) {
        this.exMulta = exMulta;
    }

}
