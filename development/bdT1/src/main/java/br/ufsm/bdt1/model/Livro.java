/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.model;

import br.ufsm.bdt1.persistencia.LivroDao;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Bruning
 */
@Entity
@Table(name = "livro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Livro.findAll", query = "SELECT l FROM Livro l")
    ,
    @NamedQuery(name = "Livro.findByIdLivro", query = "SELECT l FROM Livro l WHERE l.idLivro = :idLivro")
    ,
    @NamedQuery(name = "Livro.findByTitulo", query = "SELECT l FROM Livro l WHERE l.titulo = :titulo")
    ,
    @NamedQuery(name = "Livro.findByArea", query = "SELECT l FROM Livro l WHERE l.area = :area")
    ,
    @NamedQuery(name = "Livro.findByDisponibilidade", query = "SELECT l FROM Livro l WHERE l.disponibilidade = :disponibilidade")})
public class Livro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idLivro")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idLivro;
    @Size(max = 100)
    @Column(name = "titulo")
    private String titulo;
    @Size(max = 80)
    @Column(name = "area")
    private String area;
    @Column(name = "disponibilidade")
    private Boolean disponibilidade;
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "livroCollection")
    private Collection<Autor> autorCollection;
    @OneToMany(mappedBy = "livroidLivro")
    private Collection<Emprestimo> emprestimoCollection;
    @OneToMany(mappedBy = "livroidLivro")
    private Collection<Reserva> reservaCollection;

    public Livro() {
    }

    public Livro(Integer idLivro) {
        this.idLivro = idLivro;
    }

    public Integer getIdLivro() {
        return idLivro;
    }

    public void setIdLivro(Integer idLivro) {
        this.idLivro = idLivro;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Boolean getDisponibilidade() {
        return disponibilidade;
    }

    public void setDisponibilidade(Boolean disponibilidade) {
        this.disponibilidade = disponibilidade;
    }

    @XmlTransient
    public Collection<Autor> getAutorCollection() {
        return autorCollection;
    }

    public void setAutorCollection(Collection<Autor> autorCollection) {
        this.autorCollection = autorCollection;
    }

    @XmlTransient
    public Collection<Emprestimo> getEmprestimoCollection() {
        return emprestimoCollection;
    }

    public void setEmprestimoCollection(Collection<Emprestimo> emprestimoCollection) {
        this.emprestimoCollection = emprestimoCollection;
    }

    @XmlTransient
    public Collection<Reserva> getReservaCollection() {
        return reservaCollection;
    }

    public void setReservaCollection(Collection<Reserva> reservaCollection) {
        this.reservaCollection = reservaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLivro != null ? idLivro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Livro)) {
            return false;
        }
        Livro other = (Livro) object;
        if ((this.idLivro == null && other.idLivro != null) || (this.idLivro != null && !this.idLivro.equals(other.idLivro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.ufsm.bdt1.model.Livro[ idLivro=" + idLivro + " ]";
    }
    // <editor-fold defaultstate="collapsed" desc="Metodos Dao">

    public boolean cadastrar(Livro a) {
        if (new LivroDao().salvar(a) == true) {
            return true;
        } else {
            return false;

        }
    }

    public boolean alterar(Livro a) {
        if (new LivroDao().update(a) == true) {
            return true;
        } else {
            return false;
        }
    }

    public boolean excluir(Livro a) {
        if (new LivroDao().deletar(a) == true) {
            return true;
        } else {
            return false;
        }
    }

    //</editor-fold>
}
