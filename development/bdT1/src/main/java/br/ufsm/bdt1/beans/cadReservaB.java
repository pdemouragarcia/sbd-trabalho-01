/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.beans;

import br.ufsm.bdt1.model.Livro;
import br.ufsm.bdt1.model.Reserva;
import br.ufsm.bdt1.model.Usuario;
import br.ufsm.bdt1.persistencia.CRUD;
import java.util.ArrayList;
import java.util.Objects;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Bruning
 */
public class cadReservaB {

    /**
     * Creates a new instance of cadReservaB
     */
    private Reserva reserva = new Reserva();
    private ArrayList<Livro> livros = new ArrayList<Livro>();
    private ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
    private Usuario usuarioSelecionado;
    private Livro livroSelecionado;
    private ArrayList<Reserva> reservas = new ArrayList<Reserva>();
    private Reserva excReserva;

    public cadReservaB() {
        reserva = new Reserva();
        reservas = (ArrayList<Reserva>) CRUD.buscarTodosObjetosTabela(Reserva.class);
        livros = (ArrayList<Livro>) CRUD.buscarTodosObjetosTabela(Livro.class);
        usuarios = (ArrayList<Usuario>) CRUD.buscarTodosObjetosTabela(Usuario.class);
        livroSelecionado = new Livro();
        usuarioSelecionado = new Usuario();
        excReserva = new Reserva();
    }

    public void criarReserva() {
        if (reserva.getIdReserva() == null) {

            if (livroSelecionado.getIdLivro() != null && usuarioSelecionado.getIdUsuario() != null) {
                reserva.setLivroidLivro(livroSelecionado);
                reserva.setUsuarioidUsuario(usuarioSelecionado);
                CRUD.criar(reserva);
                atencao("Reserva cadastrada com sucesso!");
            } else {
                error("Falha ao cadastrar a reserva, preencha todos os campos!");
            }
        }else{
            atualizarReserva(reserva);
            atencao("Reserva atualizada com sucesso!");
        }

    }

    private void atualizarReserva(Reserva r){
        if(!Objects.equals(r.getLivroidLivro(), livroSelecionado))
            r.setLivroidLivro(livroSelecionado);
        if (!Objects.equals(r.getUsuarioidUsuario(), usuarioSelecionado)) 
            r.setUsuarioidUsuario(usuarioSelecionado);
        CRUD.atualizar(r);
    }
    
    public void defReserva(){
        livroSelecionado = reserva.getLivroidLivro();
        usuarioSelecionado = reserva.getUsuarioidUsuario();
    }
    public void excluirReserva() {
        if (excReserva != null) {
            CRUD.excluir(excReserva.getIdReserva(), excReserva.getClass());
            atencao("Reserva excluida com sucesso!");
        }
    }

    public void atualizarReservas() {
        reservas.clear();
        reservas = (ArrayList<Reserva>) CRUD.buscarTodosObjetosTabela(Reserva.class);
    }

    public void error(String a) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro!", a));
    }

    public void atencao(String a) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Atenção!", a));
    }

    /**
     * @return the reserva
     */
    public Reserva getReserva() {
        return reserva;
    }

    /**
     * @param reserva the reserva to set
     */
    public void setReserva(Reserva reserva) {
        this.reserva = reserva;
    }

    /**
     * @return the livros
     */
    public ArrayList<Livro> getLivros() {
        return livros;
    }

    /**
     * @param livros the livros to set
     */
    public void setLivros(ArrayList<Livro> livros) {
        this.livros = livros;
    }

    /**
     * @return the usuarios
     */
    public ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }

    /**
     * @param usuarios the usuarios to set
     */
    public void setUsuarios(ArrayList<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    /**
     * @return the usuarioSelecionado
     */
    public Usuario getUsuarioSelecionado() {
        return usuarioSelecionado;
    }

    /**
     * @param usuarioSelecionado the usuarioSelecionado to set
     */
    public void setUsuarioSelecionado(Usuario usuarioSelecionado) {
        this.usuarioSelecionado = usuarioSelecionado;
    }

    /**
     * @return the livroSelecionado
     */
    public Livro getLivroSelecionado() {
        return livroSelecionado;
    }

    /**
     * @param livroSelecionado the livroSelecionado to set
     */
    public void setLivroSelecionado(Livro livroSelecionado) {
        this.livroSelecionado = livroSelecionado;
    }

    /**
     * @return the reservas
     */
    public ArrayList<Reserva> getReservas() {
        return reservas;
    }

    /**
     * @param reservas the reservas to set
     */
    public void setReservas(ArrayList<Reserva> reservas) {
        this.reservas = reservas;
    }

    /**
     * @return the excReserva
     */
    public Reserva getExcReserva() {
        return excReserva;
    }

    /**
     * @param excReserva the excReserva to set
     */
    public void setExcReserva(Reserva excReserva) {
        this.excReserva = excReserva;
    }

}
