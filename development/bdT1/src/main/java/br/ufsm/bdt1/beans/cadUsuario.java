/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.beans;

import br.ufsm.bdt1.model.Usuario;
import br.ufsm.bdt1.persistencia.UsuarioDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.context.RequestContext;

/**
 *
 * @author pedro
 */
@ManagedBean(name = "usuarioMB")
@ViewScoped
public class cadUsuario {

    private List<Usuario> listUsuario = new UsuarioDao().buscar();
    private Usuario usuario = new Usuario();

    @PostConstruct
    public void init() {
        listUsuario = new UsuarioDao().buscar();
    }

    public List<Usuario> getListUsuario() {
        return listUsuario;
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public void novoUsuario() {
        usuario = new Usuario();
    }

    // <editor-fold defaultstate="collapsed" desc=" Cadastrar, Alterar, Excluir ">
    public void cadastrar(ActionEvent actionEvent) {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        boolean cadastrou = false;

        if (new Usuario().cadastrar(usuario) == true) {
            cadastrou = true;
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Novo Usuario Cadastrado ", "");
            listUsuario = new UsuarioDao().buscar();
            usuario = new Usuario();
        } else {
            cadastrou = false;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Usuario Não Cadastrado ", " ");
        }

        FacesContext.getCurrentInstance().addMessage(null, message);
        context.addCallbackParam("loggedIn", cadastrou);

    }

    public void alterar(ActionEvent actionEvent) {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        boolean alterou = false;

        if (new Usuario().alterar(usuario) == true) {
            alterou = true;
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Usuario alterado com sucesso ", "");
            listUsuario = new UsuarioDao().buscar();
            usuario = new Usuario();
        } else {
            alterou = false;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Usuario Não Alterado", " ");
        }

        FacesContext.getCurrentInstance().addMessage(null, message);
        context.addCallbackParam("loggedIn", alterou);
    }

    public void excluir(ActionEvent actionEvent) {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        boolean excluiu = false;
        if (new Usuario().excluir(usuario) == true) {
            excluiu = true;
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Usuario excluido com sucesso ", "");
            listUsuario = new UsuarioDao().buscar();
            usuario = new Usuario();
        } else {
            excluiu = false;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Usuario Não Excluido", " ");
        }
        FacesContext.getCurrentInstance().addMessage(null, message);
        context.addCallbackParam("loggedIn", excluiu);
    }
    //</editor-fold>

}
