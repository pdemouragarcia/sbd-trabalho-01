/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.beans;

import br.ufsm.bdt1.model.Autor;
import br.ufsm.bdt1.model.Emprestimo;
import br.ufsm.bdt1.model.Livro;
import br.ufsm.bdt1.model.Reserva;
import br.ufsm.bdt1.persistencia.CRUD;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Bruning
 */
public class cadLivroB implements Serializable {

    private Livro livro;
    private CRUD crud;
    private String disponibilidade;
    private ArrayList<Autor> autores;
    private Autor autorSelecionado;
    private ArrayList<Autor> todosAutores;
    private Autor novoAutor;
    private ArrayList<Autor> autoresSelecionados;
    private ArrayList<Livro> livros = new ArrayList<Livro>();
    private Livro excLivro;

    /**
     * Creates a new instance of cadLivroB
     */
    public cadLivroB() {
        livro = new Livro();
        crud = new CRUD();
        autores = new ArrayList<Autor>();
        autorSelecionado = new Autor();
        todosAutores = new ArrayList<Autor>();
        novoAutor = new Autor();
        autoresSelecionados = new ArrayList<Autor>();
        livros = (ArrayList<Livro>) CRUD.buscarTodosObjetosTabela(Livro.class);
        excLivro = new Livro();
        buscarAutores();
    }

    public void error(String a) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro!", a));
    }

    public void atencao(String a) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Atenção!", a));
    }

    public void excluirLivro() {
        for (Autor autore : excLivro.getAutorCollection()) {
            for (Livro l : autore.getLivroCollection()) {
                if (Objects.equals(l.getIdLivro(), excLivro.getIdLivro())) {
                    autore.getLivroCollection().remove(excLivro);
                    CRUD.atualizar(autore);
                    break;
                }
            }
        }
        if (CRUD.excluir(excLivro.getIdLivro(), excLivro.getClass())) {
            atencao("Livro excluido com sucesso!");
        } else {
            atencao("Não foi possível exvluir este livro, ele está cadastrado a um emprestimo ou reverva!");
        }
    }

    public void defLivro() {
        autoresSelecionados.addAll(livro.getAutorCollection());
    }

    public void criarAutor() {
        if (getNovoAutor() != null) {
            novoAutor.setLivroCollection(new ArrayList<Livro>());
            crud.criar(novoAutor);
            autores.add(novoAutor);
        }
    }

    public void definirAutores() {
        if (!autoresSelecionados.isEmpty()) {
            autores.addAll(autoresSelecionados);
        }
    }

    public void buscarAutores() {
        getTodosAutores().addAll((ArrayList) crud.buscarTodosObjetosTabela(Autor.class));
    }

    public void buscarLivros() {
        livros = (ArrayList<Livro>) CRUD.buscarTodosObjetosTabela(Livro.class);
    }

    public void excluirAutor() {
        if (!autores.isEmpty() || autorSelecionado == null) {
            for (Autor a : autores) {
                if (Objects.equals(a.getIdAutor(), autorSelecionado.getIdAutor())) {
                    autores.remove(a);
                    atencao("Autor excluído do livro com sucesso!");
                    break;
                }
            }
        } else {
            error("Selecione um autor para ser excluido");
        }
    }

    public void addLivro() {
        if (livro.getIdLivro() == null) {
            if (getLivro() != null) {
                if (disponibilidade.equalsIgnoreCase("0")) {
                    getLivro().setDisponibilidade(false);
                } else {
                    getLivro().setDisponibilidade(true);
                }
                livro.setAutorCollection(autores);
                this.definirLivroAutor(livro);
                //CRUD.criar(livro);
                atencao("Livro cadastrado com sucesso!");
            } else {
                error("Não foi possível cadastrar o livro!");
            }
        } else {
            atualizarLivro();
        }
        autores = new ArrayList<Autor>();
        livro = new Livro();
    }

    public void atualizarLivro() {
        livro.setAutorCollection(autoresSelecionados);
        CRUD.atualizar(livro);
        atencao("Livro atualizado com sucesso!");
    }

    public void definirLivroAutor(Livro l) {
        for (Autor autor : autores) {
            autor.getLivroCollection().add(l);
            crud.atualizar(autor);
        }
    }

    /**
     * @return the disponibilidade
     */
    public String getDisponibilidade() {
        return disponibilidade;
    }

    /**
     * @param disponibilidade the disponibilidade to set
     */
    public void setDisponibilidade(String disponibilidade) {
        this.disponibilidade = disponibilidade;
    }

    /**
     * @return the livro
     */
    public Livro getLivro() {
        return livro;
    }

    /**
     * @param livro the livro to set
     */
    public void setLivro(Livro livro) {
        this.livro = livro;
    }

    /**
     * @return the autores
     */
    public ArrayList<Autor> getAutores() {
        return autores;
    }

    /**
     * @param autores the autores to set
     */
    public void setAutores(ArrayList<Autor> autores) {
        this.autores = autores;
    }

    /**
     * @return the autorSelecionado
     */
    public Autor getAutorSelecionado() {
        return autorSelecionado;
    }

    /**
     * @param autorSelecionado the autorSelecionado to set
     */
    public void setAutorSelecionado(Autor autorSelecionado) {
        this.autorSelecionado = autorSelecionado;
    }

    /**
     * @return the todosAutores
     */
    public ArrayList<Autor> getTodosAutores() {
        return todosAutores;
    }

    /**
     * @param todosAutores the todosAutores to set
     */
    public void setTodosAutores(ArrayList<Autor> todosAutores) {
        this.todosAutores = todosAutores;
    }

    /**
     * @return the novoAutor
     */
    public Autor getNovoAutor() {
        return novoAutor;
    }

    /**
     * @param novoAutor the novoAutor to set
     */
    public void setNovoAutor(Autor novoAutor) {
        this.novoAutor = novoAutor;
    }

    /**
     * @return the autoresSelecionados
     */
    public ArrayList<Autor> getAutoresSelecionados() {
        return autoresSelecionados;
    }

    /**
     * @param autoresSelecionados the autoresSelecionados to set
     */
    public void setAutoresSelecionados(ArrayList<Autor> autoresSelecionados) {
        this.autoresSelecionados = autoresSelecionados;
    }

    /**
     * @return the livros
     */
    public ArrayList<Livro> getLivros() {
        return livros;
    }

    /**
     * @param livros the livros to set
     */
    public void setLivros(ArrayList<Livro> livros) {
        this.livros = livros;
    }

    /**
     * @return the excLivro
     */
    public Livro getExcLivro() {
        return excLivro;
    }

    /**
     * @param excLivro the excLivro to set
     */
    public void setExcLivro(Livro excLivro) {
        this.excLivro = excLivro;
    }

}
