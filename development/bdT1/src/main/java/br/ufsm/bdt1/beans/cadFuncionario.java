/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.beans;

import br.ufsm.bdt1.model.Funcionario;
import br.ufsm.bdt1.model.Usuario;
import br.ufsm.bdt1.persistencia.CRUD;
import br.ufsm.bdt1.persistencia.FuncionarioDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.context.RequestContext;

/**
 *
 * @author pedro
 */
@ManagedBean(name = "funcionarioMB")
@ViewScoped
public class cadFuncionario {

    private List<Funcionario> listFuncionario = new FuncionarioDao().buscar();
    private Funcionario funcionario = new Funcionario();
    private Usuario user = new Usuario();

    @PostConstruct
    public void init() {
        listFuncionario = new FuncionarioDao().buscar();
    }
    public void atualizarFuncionarios() {
        listFuncionario.clear();
        listFuncionario = new FuncionarioDao().buscar();
    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }
    

    public List<Funcionario> getListFuncionario() {
        return listFuncionario;
    }

    public Funcionario getFuncionario() {
        return this.funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public void novoFuncionario() {
        funcionario = new Funcionario();
    }

    // <editor-fold defaultstate="collapsed" desc=" Cadastrar, Alterar, Excluir ">
    public void cadastrar(ActionEvent actionEvent) {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        boolean cadastrou = false;
        funcionario.setUsuarioidUsuario(user);
        CRUD.criar2(user, funcionario);
        cadastrou = true;
        message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Novo Funcionario Cadastrado ", "");
        listFuncionario = new FuncionarioDao().buscar();
        funcionario = new Funcionario();
        user = new Usuario();
        FacesContext.getCurrentInstance().addMessage(null, message);
        context.addCallbackParam("loggedIn", cadastrou);

    }

    public void alterar(ActionEvent actionEvent) {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        boolean alterou = false;

        if (new Funcionario().alterar(funcionario) == true) {
            alterou = true;
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Funcionario alterado com sucesso ", "");
            listFuncionario = new FuncionarioDao().buscar();
            funcionario = new Funcionario();
        } else {
            alterou = false;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Funcionario Não Alterado", " ");
        }

        FacesContext.getCurrentInstance().addMessage(null, message);
        context.addCallbackParam("loggedIn", alterou);
    }

    public void excluir(ActionEvent actionEvent) {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        boolean excluiu = false;
        if (new Funcionario().excluir(funcionario) == true) {
            excluiu = true;
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Funcionario excluido com sucesso ", "");
            listFuncionario = new FuncionarioDao().buscar();
            funcionario = new Funcionario();
        } else {
            excluiu = false;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Funcionario Não Excluido", " ");
        }
        FacesContext.getCurrentInstance().addMessage(null, message);
        context.addCallbackParam("loggedIn", excluiu);
    }
    //</editor-fold>

}
