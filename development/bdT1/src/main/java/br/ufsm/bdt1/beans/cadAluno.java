/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.beans;

import br.ufsm.bdt1.model.Aluno;
import br.ufsm.bdt1.model.Usuario;
import br.ufsm.bdt1.persistencia.AlunoDao;
import br.ufsm.bdt1.persistencia.CRUD;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.context.RequestContext;

/**
 *
 * @author pedro
 */
@ManagedBean(name = "alunoMB")
@ViewScoped
public class cadAluno {

    private List<Aluno> listAluno = new AlunoDao().buscar();
    private Aluno aluno = new Aluno();
    private Usuario user = new Usuario();

    @PostConstruct
    public void init() {
        listAluno = new AlunoDao().buscar();
    }

    public void atualizarAlunos() {
        listAluno.clear();
        listAluno = new AlunoDao().buscar();
    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    public List<Aluno> getListAluno() {
        return listAluno;
    }

    public Aluno getAluno() {
        return this.aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public void novoAluno() {
        aluno = new Aluno();
    }

    public void novoUsuario() {
        user = new Usuario();
    }

    // <editor-fold defaultstate="collapsed" desc=" Cadastrar, Alterar, Excluir ">
    public void cadastrar(ActionEvent actionEvent) {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        boolean cadastrou = false;
        aluno.setUsuarioidUsuario(user);
        CRUD.criar2(user, aluno);
        cadastrou = true;
        message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Novo Aluno Cadastrado ", "");
        listAluno = new AlunoDao().buscar();
        aluno = new Aluno();
        user = new Usuario();
        FacesContext.getCurrentInstance().addMessage(null, message);
        context.addCallbackParam("loggedIn", cadastrou);

    }

    public void alterar(ActionEvent actionEvent) {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        boolean alterou = false;

        if (new Aluno().alterar(aluno) == true) {
            alterou = true;
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Aluno alterado com sucesso ", "");
            listAluno = new AlunoDao().buscar();
            aluno = new Aluno();
        } else {
            alterou = false;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Aluno Não Alterado", " ");
        }

        FacesContext.getCurrentInstance().addMessage(null, message);
        context.addCallbackParam("loggedIn", alterou);
    }

    public void excluir(ActionEvent actionEvent) {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        boolean excluiu = false;
        if (new Aluno().excluir(aluno) == true) {
            excluiu = true;
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Aluno excluido com sucesso ", "");
            listAluno = new AlunoDao().buscar();
            aluno = new Aluno();
        } else {
            excluiu = false;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Aluno Não Excluido", " ");
        }
        FacesContext.getCurrentInstance().addMessage(null, message);
        context.addCallbackParam("loggedIn", excluiu);
    }
    //</editor-fold>
}
