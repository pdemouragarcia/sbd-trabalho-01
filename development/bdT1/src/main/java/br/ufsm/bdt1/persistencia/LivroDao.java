/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.persistencia;

import br.ufsm.bdt1.model.Livro;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class LivroDao extends Dao implements Serializable {

    // <editor-fold defaultstate="collapsed" desc=" Buscar ">
    public ArrayList<Livro> buscar() {
        return (ArrayList<Livro>) buscarObjetos(Livro.class);
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Inserir ">
    public boolean salvar(Livro a) {
        return super.salvar(a);
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Remover ">
    public boolean deletar(Livro a) {
        return excluir(a);
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Alterar ">
    public boolean update(Livro a) {
        return super.update(a);
    }
    //</editor-fold>
}
