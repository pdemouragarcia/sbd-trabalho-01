/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.beans;

import br.ufsm.bdt1.model.Usuario;
import br.ufsm.bdt1.persistencia.CRUD;
import java.util.HashMap;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Bruning
 */
public class loginB {

    /**
     * Creates a new instance of loginB
     */
    private Usuario usuario, user;
    private CRUD crud;

    public loginB() {
        usuario = new Usuario();
        crud = new CRUD();
        user = new Usuario();
    }

    public String verificarUsuario() {
        HashMap<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("cpf", usuario.getCpf());
        parametros.put("senha", usuario.getSenha());
        user = (Usuario) crud.buscarObjetoViaListaParametros(parametros, Usuario.class);

        if (user != null) {
            if (usuario != null) {
                if (usuario.getCpf().equals(user.getCpf()) && usuario.getSenha().equals(user.getSenha())) {
                    return "index";
                } else {
                    error("CPF ou senha incorretos.");
                    return "login";
                }
            } else {
                atencao("É necessário digitar o CPF e senha para acessar.");
                return "login";
            }
        } else {
            error("Usuário inexistente.");
            return "login";
        }
    }

    public void error(String a) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro!", a));
    }

    public void atencao(String a) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Atenção!", a));
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

}
