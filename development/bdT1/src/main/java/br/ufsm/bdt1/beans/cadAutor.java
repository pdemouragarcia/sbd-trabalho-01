/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.bdt1.beans;

import br.ufsm.bdt1.model.Autor;
import br.ufsm.bdt1.persistencia.AutorDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.context.RequestContext;

/**
 *
 * @author pedro
 */
@ManagedBean(name = "autorMB")
@ViewScoped
public class cadAutor {

    private List<Autor> listAutor = new AutorDao().buscar();
    private Autor autor = new Autor();

    @PostConstruct
    public void init() {
        listAutor = new AutorDao().buscar();
    }

    public List<Autor> getListAutor() {
        return listAutor;
    }

    public Autor getAutor() {
        return this.autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }
    
    public void novoAutor(){
        autor = new Autor();
    }

    // <editor-fold defaultstate="collapsed" desc=" Cadastrar, Alterar, Excluir ">
    public void cadastrar(ActionEvent actionEvent) {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        boolean cadastrou = false;

        if (new Autor().cadastrar(autor) == true) {
            cadastrou = true;
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Novo Autor Cadastrado ", "");
            listAutor = new AutorDao().buscar();
            autor = new Autor();
        } else {
            cadastrou = false;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Autor Não Cadastrado ", " ");
        }

        FacesContext.getCurrentInstance().addMessage(null, message);
        context.addCallbackParam("loggedIn", cadastrou);

    }

    public void alterar(ActionEvent actionEvent) {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        boolean alterou = false;

        if (new Autor().alterar(autor) == true) {
            alterou = true;
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Autor alterado com sucesso ", "");
            listAutor = new AutorDao().buscar();
            autor = new Autor();
        } else {
            alterou = false;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Autor Não Alterado", " ");
        }

        FacesContext.getCurrentInstance().addMessage(null, message);
        context.addCallbackParam("loggedIn", alterou);
    }

    public void excluir(ActionEvent actionEvent) {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        boolean excluiu = false;
        if (new Autor().excluir(autor) == true) {
            excluiu = true;
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Autor excluido com sucesso ", "");
            listAutor = new AutorDao().buscar();
            autor = new Autor();
        } else {
            excluiu = false;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Autor Não Excluido", " ");
        }
        FacesContext.getCurrentInstance().addMessage(null, message);
        context.addCallbackParam("loggedIn", excluiu);
    }
    //</editor-fold>

}
